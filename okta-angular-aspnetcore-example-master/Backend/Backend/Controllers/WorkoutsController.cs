﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Models;

namespace Backend.Controllers
{
    [Produces("application/json")]
    [Route("api/Workouts")]
    public class WorkoutsController : Controller
    {
        public WorkoutsController()
        {
        }

        // GET: api/Workouts
        [HttpGet]
        public IEnumerable<Workout> GetWorkout()
        {
            var client = new WebClient();
            client.Headers.Add("Accept", "application/json");

            var result = client.DownloadString
                ("http://localhost:47354/TweetService.svc/GetWorkouts");

            var serializer = new DataContractJsonSerializer(typeof(List<Workout>));

            List<Workout> workouts;
            using (var stream = new MemoryStream(Encoding.ASCII.GetBytes(result)))
            {
                workouts = (List<Workout>)serializer.ReadObject(stream);
            }

            return workouts;
        }

        // GET: api/Workouts/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetWorkout([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var client = new WebClient();
            client.Headers.Add("Accept", "application/json");

            var result = client.DownloadString
                ("http://localhost:47354/TweetService.svc/GetWorkouts");

            var serializer = new DataContractJsonSerializer(typeof(List<Workout>));

            List<Workout> workouts;
            using (var stream = new MemoryStream(Encoding.ASCII.GetBytes(result)))
            {
                workouts = (List<Workout>)serializer.ReadObject(stream);
            }

            return Ok(workouts);
        }

        // PUT: api/Workouts/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutWorkout([FromRoute] int id, [FromBody] Workout workout)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != workout.Id)
            {
                return BadRequest();
            }

            

            return NoContent();
        }

        // POST: api/Workouts
        [HttpPost]
        public async Task<IActionResult> PostWorkout([FromBody] Workout workout)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var workoutFromServer = SendDataToServer(
                "http://localhost:47354/TweetService.svc/CreateWorkout",
                "POST", workout);

            return CreatedAtAction("GetWorkout", new { id = workoutFromServer.Id }, workout);
        }

        // DELETE: api/Workouts/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteWorkout([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var workoutFromServer = SendDataToServer(
                "http://localhost:47356/TweetService.svc/DeleteWorkout",
                "DELETE", id);

            return Ok(workoutFromServer);
        }

        private T SendDataToServer<T>(string endpoint, string method, T workout)
        {
            // Endpoit - is the URI we will be dealing with
            var request = (HttpWebRequest)WebRequest.Create(endpoint);

            // Indicate that we are dealing with JSON object
            // to the Web Server
            request.Accept = "application/json";
            request.ContentType = "application/json";

            // HTTP Verb Method Type: Get/Put/Post
            request.Method = method;

            // Working with Request and Response Streams directly           
            var serializer = new DataContractJsonSerializer(typeof(T));

            // Stream that represents the data that will be sent to the Server
            // This will open a Stream connection (Network Stream that
            // we can write too) to the Server 
            var requestStream = request.GetRequestStream();

            // Write the Request Stream
            // Serialize and write a .net object (in memory: tweet) to JSON 
            serializer.WriteObject(requestStream, workout);

            // Close the request stream
            requestStream.Close();

            var response = request.GetResponse();
            if (response.ContentLength == 0)
            {
                response.Close();
                return default(T);
            }

            // Deserialize from JSON object back to a .net object
            var responseStream = response.GetResponseStream();
            var responseObject = (T)serializer.ReadObject(responseStream);

            responseStream.Close();

            return responseObject;
        }
    }
}