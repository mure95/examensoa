﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace WorkoutDAL
{
    interface IDataAccessMethods
    {
        IEnumerable<Workout> GetWorkouts();
        Task<Workout> GetWorkout(int id);
        Task UpdateWorkout(int id, Workout workout);
        Task<Workout> AddWorkout(Workout workout);
        Task<Workout> DeleteWorkout(int id);
    }
}
