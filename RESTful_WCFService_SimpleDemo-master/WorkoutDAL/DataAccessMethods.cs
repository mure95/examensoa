﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;

namespace WorkoutDAL
{
    public class DataAccessMethods : IDataAccessMethods
    {
        private readonly WorkoutContext _context;

        public DataAccessMethods()
        {
            _context = new WorkoutContext();
        }

        public IEnumerable<Workout> GetWorkouts()
        {
            return _context.Workout.OrderByDescending(d => d.Date);
        }

        public async Task<Workout> GetWorkout(int id)
        {
            return await _context.Workout.SingleOrDefaultAsync(m => m.Id == id);
        }

        public async Task UpdateWorkout(int id, Workout workout)
        {
            var workoutFromDb = _context.Workout.FirstOrDefault(c => c.Id == id);
            if (workoutFromDb == null)
                //return NotFound();

            workoutFromDb.Date = workout.Date;
            workoutFromDb.DistanceInMeters = workout.DistanceInMeters;
            workoutFromDb.TimeInSeconds = workout.TimeInSeconds;
            try
            {
                _context.Workout.AddOrUpdate(workoutFromDb);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!WorkoutExists(id))
                {
                    //return NotFound();
                }
                else
                {
                    throw;
                }
            }
        }

        public async Task<Workout> AddWorkout(Workout workout)
        {
            _context.Workout.Add(workout);
            await _context.SaveChangesAsync();

            return workout;
        }

        public async Task<Workout> DeleteWorkout(int id)
        {
            var workout = await _context.Workout.SingleOrDefaultAsync(m => m.Id == id);
            if (workout == null)
            {
                return null;
            }

            _context.Workout.Remove(workout);
            await _context.SaveChangesAsync();

            return workout;
        }

        private bool WorkoutExists(int id)
        {
            return _context.Workout.Any(e => e.Id == id);
        }
    }
}
