﻿using System.Data.Entity;

namespace WorkoutDAL
{
    public class WorkoutContext : DbContext
    {
        public WorkoutContext() : base("WorkoutContext")
        {
        }

        public DbSet<Workout> Workout { get; set; }
    }
}
