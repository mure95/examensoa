﻿using System.Linq;
using System.Threading.Tasks;
using static System.Int32;

namespace Tweet.WCFService.RESTful
{
    using System.Collections.Generic;
    using System.ServiceModel;
    using System.ServiceModel.Activation;
    using System.ServiceModel.Web;
    using WorkoutDAL;

    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements
        (RequirementsMode = 
        AspNetCompatibilityRequirementsMode.Allowed)]
    public class TweetService
    {         
        private DataAccessMethods _dataAccessMethods;

        public TweetService()
        {
            _dataAccessMethods = new DataAccessMethods();
        }
     
        [WebGet(UriTemplate = "/GetWorkouts")]
        public IList<Workout> GetWorkouts()
        {
            return _dataAccessMethods.GetWorkouts().ToList();
        }


        [WebInvoke(UriTemplate = "/CreateWorkout")]
        public async Task<Workout> CreateWorkout(Workout newWorkout)
        {
            return await _dataAccessMethods.AddWorkout(newWorkout);
        }

        //[WebInvoke(Method = "PUT", UriTemplate = "/Tweet/{id}")]
        //public async Task UpdateTweet(string workoutId, Workout updateWorkout)
        //{
        //    TryParse(workoutId, out var workoutIdParsedToInt);
        //    await _dataAccessMethods.UpdateWorkout(workoutIdParsedToInt, updateWorkout);
        //}

        [WebInvoke(Method = "DELETE", UriTemplate = "/DeleteWorkout")]
        public async Task<Workout> DeleteWorkout(string deleteTweetId)
        {
            TryParse(deleteTweetId, out var deleteWorkoutIdParsedToInt);

            return await _dataAccessMethods.DeleteWorkout(deleteWorkoutIdParsedToInt);
        }
    }
}
